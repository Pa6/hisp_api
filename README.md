Step to install this project


After cloning, follow this steps:
Step 1: go to the project directory  

Step 2: Update dependency by typing this command: composer update

Step 3: Run the project on port 8000 by typing php artisan serve port=8000

Step 4: Call the api in browser or in postman http://localhost:8000/api/get-price [it is a get method to use]

