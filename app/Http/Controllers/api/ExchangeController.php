<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{

    #API CALL FOR COINMARKET HISTORICAL DATA
    public function getHistoricalPrice(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('historical_data'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'X-CMC_PRO_API_KEY: b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);



        #json decoder
        $json = json_decode($response, true);
        #return api data
        return $json['data']['1765']['quotes'];
    }

}
